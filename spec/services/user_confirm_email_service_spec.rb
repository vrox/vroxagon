# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserConfirmEmailService do
  subject(:service_response) { described_class.new(verification_token).execute }

  let(:user) { create(:user, unconfirmed_email: true) }

  context 'with a valid verification token' do
    let(:verification_token) { user.email_verification_token }

    it { is_expected.to be_success }

    it 'sets the user to verified' do
      service_response
      expect(user.reload.email_confirmed?).to be(true)
    end
  end

  context 'with an invalid verification token' do
    let(:verification_token) { 'someRandomString' }

    it { is_expected.not_to be_success }
    it { expect(service_response.message).to eq('Invalid verification token') }
  end

  context 'with invalid input' do
    let(:verification_token) { nil }

    it { is_expected.not_to be_success }
    it { expect(service_response.message).to eq('Missing verification token') }
  end
end
