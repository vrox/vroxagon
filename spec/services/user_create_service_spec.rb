# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserCreateService do
  subject(:service_response) { described_class.new(username, email, password).execute }

  context 'when user is valid' do
    let(:username) { 'test' }
    let(:email) { 'test@vrox.eu' }
    let(:password) { 'test_password' }

    it { is_expected.to be_success }
    it { expect(service_response.payload).to be_valid }
    it('sets username correct') { expect(service_response.payload.username).to eq(username) }
    it('sets email correct') { expect(service_response.payload.email).to eq(email) }
    it('sets password correct') { expect(service_response.payload.password).to eq(password) }
    it('creates a email_verification_token') { expect(service_response.payload.email_verification_token).to be_present }

    it_behaves_like 'sends an email' do
      let(:mailer_class) { UserMailer }
      let(:mail_method) { :registered_email }
      let(:mail_params) { { user: instance_of(User) } }
    end
  end

  context 'when user is invalid' do
    let!(:user_with_username) { create(:user, username: 'user') }
    let!(:user_with_email) { create(:user, email: 'test@vrox.eu') }

    context 'when username is duplicated' do
      let(:username) { user_with_username.username }
      let(:email) { generate(:email) }
      let(:password) { generate(:password) }

      it { is_expected.not_to be_success }
      it { expect(service_response.message).to eq('username is already taken') }

      it_behaves_like 'sends no email' do
        let(:mailer_class) { UserMailer }
        let(:mail_method) { :registered_email }
        let(:mail_params) { { user: instance_of(User) } }
      end
    end

    context 'when email is duplicated' do
      let(:username) { generate(:username) }
      let(:email) { user_with_email.email }
      let(:password) { generate(:password) }

      it { is_expected.not_to be_success }
      it { expect(service_response.message).to eq('email is already taken') }

      it_behaves_like 'sends no email' do
        let(:mailer_class) { UserMailer }
        let(:mail_method) { :registered_email }
        let(:mail_params) { { user: instance_of(User) } }
      end
    end
  end

  context 'when invalid inputs are given' do
    context 'when username is nil' do
      let(:username) { nil }
      let(:email) { generate(:email) }
      let(:password) { generate(:password) }

      it { is_expected.not_to be_success }
      it { expect(service_response.message).to eq('User is invalid') }

      it_behaves_like 'sends no email' do
        let(:mailer_class) { UserMailer }
        let(:mail_method) { :registered_email }
        let(:mail_params) { { user: instance_of(User) } }
      end
    end

    context 'when email is nil' do
      let(:username) { generate(:username) }
      let(:email) { nil }
      let(:password) { generate(:password) }

      it { is_expected.not_to be_success }
      it { expect(service_response.message).to eq('User is invalid') }

      it_behaves_like 'sends no email' do
        let(:mailer_class) { UserMailer }
        let(:mail_method) { :registered_email }
        let(:mail_params) { { user: instance_of(User) } }
      end
    end

    context 'when password is nil' do
      let(:username) { generate(:username) }
      let(:email) { generate(:email) }
      let(:password) { nil }

      it { is_expected.not_to be_success }
      it { expect(service_response.message).to eq('User is invalid') }

      it_behaves_like 'sends no email' do
        let(:mailer_class) { UserMailer }
        let(:mail_method) { :registered_email }
        let(:mail_params) { { user: instance_of(User) } }
      end
    end
  end
end
