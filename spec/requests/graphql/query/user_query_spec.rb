# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'user Query' do
  include GraphqlHelpers

  let(:query) do
    <<~QUERY
      query($userId: UserID!) {
        user(id: $userId) {
          id
        }
      }
    QUERY
  end

  let(:user_id) { nil }
  let(:variables) { { userId: user_id } }

  before { post_graphql query, variables: variables }

  context 'without an id' do
    it 'returns an error', :aggregate_failures do
      expect(graphql_data_at(:user)).to be_nil
      expect(graphql_errors).not_to be_empty
    end
  end

  context 'with an invalid id' do
    let(:user_id) { 'gid://vroxagon/Users/1' }

    it 'returns an error', :aggregate_failures do
      expect(graphql_data_at(:user)).to be_nil
      expect(graphql_errors).not_to be_empty
    end
  end

  context 'with a valid id but out of range' do
    let(:user_id) { 'gid://vroxagon/User/0' }

    it 'returns only nil', :aggregate_failures do
      expect(graphql_data_at(:user)).to be_nil
      expect_graphql_errors_to_be_empty
    end
  end

  context 'with a valid id' do
    let(:user) { create(:user) }
    let(:user_id) { "gid://vroxagon/User/#{user.id}" }

    it 'returns the user' do
      expect(graphql_data_at(:user, :id)).to eq(user.to_global_id.to_s)
    end
  end
end
