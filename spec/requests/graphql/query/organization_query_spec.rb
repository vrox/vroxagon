# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'organization Query' do
  include GraphqlHelpers

  let(:query) do
    <<~QUERY
      query($organizationId: OrganizationID!) {
        organization(id: $organizationId) {
          id
          name
          path
          visibility
        }
      }
    QUERY
  end

  let(:organization_id) { nil }
  let(:variables) { { organizationId: organization_id } }

  before { post_graphql query, variables: variables }

  context 'without an id' do
    it 'returns an error', :aggregate_failures do
      expect(graphql_data_at(:graphql)).to be_nil
      expect(graphql_errors).not_to be_empty
    end
  end

  context 'with an invalid id' do
    let(:organization_id) { 'gid://vroxagon/Organizations/1' }

    it 'returns an error', :aggregate_failures do
      expect(graphql_data_at(:organization)).to be_nil
      expect(graphql_errors).not_to be_empty
    end
  end

  context 'with a valid id but out of range' do
    let(:organization_id) { 'gid://vroxagon/Organization/0' }

    it 'returns only nil', :aggregate_failures do
      expect(graphql_data_at(:organization)).to be_nil
      expect_graphql_errors_to_be_empty
    end
  end

  context 'with a valid id' do
    let(:organization) { create(:organization) }
    let(:organization_id) { "gid://vroxagon/Organization/#{organization.id}" }

    it 'returns the organization', :aggregate_failures do
      expect(graphql_data_at(:organization, :id)).to eq(organization.to_global_id.to_s)
      expect(graphql_data_at(:organization, :name)).to eq(organization.name)
      expect(graphql_data_at(:organization, :path)).to eq(organization.path)
      expect(graphql_data_at(:organization, :visibility)).to eq(organization.visibility)
    end
  end
end
