# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'createOrganization Mutation' do
  include GraphqlHelpers

  let(:mutation) do
    <<~QUERY
      mutation($input: CreateOrganizationInput!) {
        createOrganization(input: $input) {
          errors
          organization {
            id
          }
        }
      }
    QUERY
  end

  let(:input) do
    name = generate(:organization_name)

    {
      name: name,
      path: name,
      visibility: :public,
    }
  end

  let(:variables) { { input: input } }
  let(:current_user) { create(:user) }

  before { post_graphql mutation, variables: variables, current_user: current_user }

  it 'creates organization', :aggregate_failures do
    expect(graphql_data_at(:create_organization, :organization, :id)).to be_present

    organization = VroxagonSchema.object_from_id(graphql_data_at(:create_organization, :organization, :id),
                                                 expected_type: Organization)

    expect(organization.name).to eq(input[:name])
    expect(organization.path).to eq(input[:path])
  end
end
