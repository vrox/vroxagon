# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Graphql' do
  include GraphqlHelpers

  let(:echo_message) { 'query test' }
  let(:query) do
    <<~QUERY
      query {
        echo(message: "#{echo_message}")
      }
    QUERY
  end
  let(:authorization) { nil }
  let(:headers) { { authorization: authorization } }

  before do
    post_graphql query, headers: headers
  end

  context 'without authorization' do
    it 'resolves the query', :aggregate_failures do
      expect(response).to have_http_status(:ok)

      expect_graphql_errors_to_be_empty
      expect(graphql_data_at(:echo)).to eq(echo_message)
    end

    context 'when using mutations' do
      let(:query) do
        <<~QUERY
          mutation {
            echo(message: "#{echo_message}")
          }
        QUERY
      end

      it 'denies access' do
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  context 'with valid authorization' do
    let(:user) { create(:user) }
    let(:user_session) { create(:user_session, user: user) }
    let(:authorization) { "Session #{user_session.token}" }

    context 'when user can access the api' do
      it 'resolves the query', :aggregate_failures do
        expect(response).to have_http_status(:ok)

        expect_graphql_errors_to_be_empty
        expect(graphql_data_at(:echo)).to eq(echo_message)
      end

      context 'when using mutations' do
        let(:query) do
          <<~QUERY
            mutation {
              echo(message: "#{echo_message}")
            }
          QUERY
        end

        it 'resolves the query', :aggregate_failures do
          expect(response).to have_http_status(:ok)

          expect_graphql_errors_to_be_empty
          expect(graphql_data_at(:echo)).to eq(echo_message)
        end
      end
    end

    context 'when user cannot access the api' do
      let(:user) { create(:user, unconfirmed_email: true) }

      it 'returns forbidden', :aggregate_failures do
        expect(response).to have_http_status(:forbidden)
        expect(response.body).to be_empty
      end
    end
  end

  context 'with invalid authorization' do
    let(:authorization) { 'blub' }

    it 'returns unauthorized', :aggregate_failures do
      expect(response).to have_http_status(:unauthorized)
      expect(response.body).to be_empty
    end
  end
end
