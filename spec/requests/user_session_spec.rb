# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'UserSessions' do
  let(:user) { create(:user, username: 'username', password: 'password') }
  let(:username) { nil }
  let(:password) { nil }

  before do
    post user_sessions_path, params: { user: { username: username, password: password } }
  end

  context 'when provided credentials are valid' do
    let(:username) { user.username }
    let(:password) { user.password }

    it 'returns a UserSession for the user', :aggregate_failures do
      expect(response).to have_http_status(:ok)

      json_response = response.parsed_body
      expect(json_response).to match a_hash_including('user_id' => user.id)
    end

    it 'returns a valid token', :aggregate_failures do
      json_response = response.parsed_body
      token = json_response['token']

      session = UserSession.find_by(token: token)
      expect(session).to be_present
      expect(session.user).to eq(user)
    end

    context 'when user cannot log in' do
      let(:user) { create(:user, unconfirmed_email: true) }

      it 'return forbidden', :aggregate_failures do
        expect(response).to have_http_status(:forbidden)
        expect(response.body).to eq({ reason: :unverified_email }.to_json)
      end
    end
  end

  context 'when provided credentials are invalid' do
    context 'when username is invalid' do
      let(:username) { generate(:username) }
      let(:password) { user.password }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end

    context 'when password is invalid' do
      let(:username) { user.username }
      let(:password) { generate(:password) }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end

    context 'when username is blank' do
      let(:username) { '' }
      let(:password) { user.password }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end

    context 'when password is blank' do
      let(:username) { user.username }
      let(:password) { '' }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end

    context 'when username is nil' do
      let(:username) { nil }
      let(:password) { user.password }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end

    context 'when password is nil' do
      let(:username) { user.username }
      let(:password) { nil }

      it 'returns unauthorized', :aggregate_failures do
        expect(response).to have_http_status(:unauthorized)
        expect(response.body).to be_empty
      end
    end
  end
end
