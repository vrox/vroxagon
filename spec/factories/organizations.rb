# frozen_string_literal: true

FactoryBot.define do
  factory :organization do
    name { generate(:organization_name) }
    path { name }
    visibility { :public }
  end
end
