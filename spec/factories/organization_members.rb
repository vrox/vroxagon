# frozen_string_literal: true

FactoryBot.define do
  factory :organization_member do
    organization
    user

    Vroxagon::Role::ENUM_VALUES.entries.each_entry do |role, value|
      trait role do
        after(:create) do |organization_member|
          create(
            :organization_member_role_assignment,
            organization_member: organization_member,
            role: value
          )
          organization_member.reload
        end
      end
    end
  end
end
