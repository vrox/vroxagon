# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    username { generate(:username) }
    email { generate(:email) }
    password { 'S0mE P4SsW0RD!' }

    transient do
      unconfirmed_email { false }
    end

    after(:create) do |user, evaluator|
      UserConfirmEmailService.new(user.email_verification_token).execute unless evaluator.unconfirmed_email
      user.reload
    end
  end
end
