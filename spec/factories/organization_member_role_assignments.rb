# frozen_string_literal: true

FactoryBot.define do
  factory :organization_member_role_assignment do
    organization_member
    created_by factory: :user
    role { 0 }
  end
end
