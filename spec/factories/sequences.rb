# frozen_string_literal: true

FactoryBot.define do
  sequence(:username) { |n| "user#{n}" }
  sequence(:email) { |n| "user#{n}@vroxagon.vrox.eu" }
  sequence(:password) { |n| "#{SecureRandom.base58(10)}-#{n}" }

  sequence(:organization_name) { |n| "organization#{n}" }
end
