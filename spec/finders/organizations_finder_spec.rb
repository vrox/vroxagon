# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganizationsFinder do
  subject { described_class.new(params).execute }

  let!(:first_org) { create(:organization) }
  let!(:second_org) { create(:organization) }

  let(:params) { {} }

  context 'when no params are given' do
    it { is_expected.to contain_exactly(first_org, second_org) }
  end

  context 'when filtering by id' do
    let(:params) { { id: second_org.id } }

    it { is_expected.to contain_exactly(second_org) }
  end

  context 'when filtering by path' do
    let(:params) { { path: second_org.path } }

    it { is_expected.to contain_exactly(second_org) }
  end

  context 'when setting limit' do
    let(:params) { { limit: 1 } }

    it { is_expected.to contain_exactly(first_org) }
  end

  context 'when setting single' do
    let(:params) { { single: true } }

    it { is_expected.to eq(first_org) }

    context 'when using single_use_last' do
      let(:params) { { single: true, single_use_last: true } }

      it { is_expected.to eq(second_org) }
    end
  end
end
