# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User do
  subject { create(:user) }

  describe 'associations' do
    it { is_expected.to have_many(:user_sessions).inverse_of(:user) }
    it { is_expected.to have_many(:organization_memberships).inverse_of(:user) }
    it { is_expected.to have_many(:organizations).through(:organization_memberships) }
    it { is_expected.to have_many(:created_organization_member_role_assignments).inverse_of(:created_by) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_presence_of(:email) }

    it { is_expected.to validate_uniqueness_of(:username).case_insensitive }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }

    it { is_expected.to validate_length_of(:username).is_at_most(50) }

    context 'when `email_verification_token` is set' do
      subject { create(:user, email_verification_token: 'test') }

      it { is_expected.to validate_uniqueness_of(:email_verification_token) }
    end
  end
end
