# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganizationMember do
  subject { create(:organization_member) }

  describe 'associations' do
    it { is_expected.to belong_to(:user).required.inverse_of(:organization_memberships) }
    it { is_expected.to belong_to(:organization).required.inverse_of(:organization_members) }

    it { is_expected.to have_many(:roles).inverse_of(:organization_member) }
  end

  describe 'role predicates' do
    Vroxagon::Role::ENUM_VALUES.each_key do |role|
      context "with #{role}" do
        subject { organization_member.public_send(role_predicate) }

        let(:organization_member) { create(:organization_member, role) }
        let(:role_predicate) { "#{role}?" }

        it { is_expected.to be(true) }

        Vroxagon::Role::ENUM_VALUES.keys.excluding(role).each do |inner_role|
          context "when checking #{inner_role}" do
            let(:role_predicate) { "#{inner_role}?" }

            it { is_expected.to be(false) }
          end
        end
      end
    end
  end
end
