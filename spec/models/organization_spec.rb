# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Organization do
  subject { create(:organization) }

  describe 'associations' do
    it { is_expected.to have_many(:organization_members).inverse_of(:organization) }
    it { is_expected.to have_many(:users).through(:organization_members) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:path) }
    it { is_expected.to validate_presence_of(:visibility) }

    it { is_expected.to validate_uniqueness_of(:path).case_insensitive }
  end
end
