# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganizationMemberRoleAssignment do
  subject do
    create(:organization_member_role_assignment, organization_member: organization_member,
                                                 created_by: created_by,
                                                 role: role)
  end

  let(:organization_member) { create(:organization_member) }
  let(:created_by) { create(:user) }
  let(:role) { 0 }

  describe 'associations' do
    it { is_expected.to belong_to(:organization_member).required.inverse_of(:roles) }
    it { is_expected.to belong_to(:created_by).required.inverse_of(:created_organization_member_role_assignments) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:role) }
  end
end
