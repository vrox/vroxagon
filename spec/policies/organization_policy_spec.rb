# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganizationPolicy do
  subject { described_class.new(current_user, organization) }

  let(:current_user) { create(:user) }

  context 'when organization is public' do
    let(:organization) { create(:organization) }

    it { is_expected.to be_allowed(:read_organization) }
  end

  context 'when organization is private' do
    let(:organization) { create(:organization, visibility: :private) }

    context 'when current_user is nil' do
      let(:current_user) { nil }

      it { is_expected.to be_disallowed(:read_organization) }
    end

    context 'when current_user is not a member' do
      it { is_expected.to be_disallowed(:read_organization) }
    end

    context 'when current_user is a member' do
      before do
        create(:organization_member, organization: organization, user: current_user)
      end

      it { is_expected.to be_allowed(:read_organization) }
    end
  end
end
