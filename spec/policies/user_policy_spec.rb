# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserPolicy do
  subject { described_class.new(current_user, target_user) }

  let(:current_user) { create(:user) }
  let(:target_user) { create(:user) }

  it { is_expected.to be_allowed(:read_user) }
end
