# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GlobalPolicy do
  subject { described_class.new(current_user, :global) }

  let(:current_user) { create(:user) }

  it { is_expected.to be_allowed(:log_in) }
  it { is_expected.to be_allowed(:access_api) }

  context 'when current_user has unconfirmed email' do
    let(:current_user) { create(:user, unconfirmed_email: true) }

    it { is_expected.to be_disallowed(:log_in) }
    it { is_expected.to be_disallowed(:access_api) }
  end

  context 'when current_user is nil' do
    let(:current_user) { nil }

    it { is_expected.to be_disallowed(:log_in) }
    it { is_expected.to be_allowed(:access_api) }
  end
end
