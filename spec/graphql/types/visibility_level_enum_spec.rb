# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VroxagonSchema.types['VisibilityLevelEnum'] do
  it { expect(described_class.graphql_name).to eq('VisibilityLevelEnum') }

  it 'exposes all the existing visibility levels' do
    expect(described_class.values.keys).to match_array(%w[private public])
  end
end
