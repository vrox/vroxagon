# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VroxagonSchema.types['User'] do
  let(:fields) do
    %w[
      id
      username
      organizations
    ]
  end

  it { expect(described_class.graphql_name).to eq('User') }
  it { expect(described_class).to have_graphql_fields(fields) }
  it { expect(described_class).to require_graphql_authorizations(:read_user) }
end
