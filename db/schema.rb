# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_09_29_201501) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "organization_member_role_assignments", force: :cascade do |t|
    t.bigint "organization_member_id", null: false
    t.bigint "created_by_id", null: false
    t.integer "role", default: 0, null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index ["organization_member_id", "created_by_id"], name: "index_7c4bb161ae", unique: true
  end

  create_table "organization_members", force: :cascade do |t|
    t.bigint "organization_id", null: false
    t.bigint "user_id", null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index ["organization_id", "user_id"], name: "index_organization_members_on_organization_id_and_user_id", unique: true
    t.index ["organization_id"], name: "index_organization_members_on_organization_id"
    t.index ["user_id"], name: "index_organization_members_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.text "name", null: false
    t.text "path", null: false
    t.integer "visibility", default: 0, null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index "lower(path)", name: "index_organizations_on_LOWER_path", unique: true
    t.check_constraint "char_length(name) <= 32", name: "check_d130d769e0"
    t.check_constraint "char_length(path) <= 32", name: "check_0b4296b5ea"
  end

  create_table "user_sessions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.text "token", null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.index ["token"], name: "index_user_sessions_on_token", unique: true
    t.index ["user_id"], name: "index_user_sessions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.text "username", null: false
    t.text "email", null: false
    t.text "password_digest", null: false
    t.timestamptz "created_at", null: false
    t.timestamptz "updated_at", null: false
    t.text "email_verification_token"
    t.timestamptz "email_confirmed_at"
    t.index "lower(email)", name: "index_users_on_LOWER_email", unique: true
    t.index "lower(username)", name: "index_users_on_LOWER_username", unique: true
    t.index ["email_verification_token"], name: "index_users_on_email_verification_token", unique: true, where: "(email_verification_token IS NOT NULL)"
    t.check_constraint "char_length(email) <= 255", name: "check_3bedaaa612"
    t.check_constraint "char_length(username) <= 50", name: "check_56606ce552"
  end

  add_foreign_key "organization_member_role_assignments", "organization_members", on_delete: :cascade
  add_foreign_key "organization_member_role_assignments", "users", column: "created_by_id", on_delete: :cascade
  add_foreign_key "organization_members", "organizations", on_delete: :cascade
  add_foreign_key "organization_members", "users", on_delete: :cascade
  add_foreign_key "user_sessions", "users", on_delete: :cascade
end
