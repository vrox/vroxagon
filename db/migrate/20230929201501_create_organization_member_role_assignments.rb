# frozen_string_literal: true

class CreateOrganizationMemberRoleAssignments < Vroxagon::Database::Migration[1.0]
  def change
    create_table :organization_member_role_assignments do |t, helper|
      t.belongs_to :organization_member, null: false, foreign_key: { on_delete: :cascade }, index: false
      t.belongs_to :created_by, null: false, foreign_key: { on_delete: :cascade, to_table: :users }, index: false
      t.integer :role, null: false, default: 0
      t.index %i[organization_member_id created_by_id],
              unique: true,
              name: helper.index_name(:organization_member_role_assignments, %i[organization_member created_by],
                                      :foreign_key)

      t.timestamps_with_timezone
    end
  end
end
