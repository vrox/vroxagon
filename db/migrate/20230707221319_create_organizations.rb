# frozen_string_literal: true

class CreateOrganizations < Vroxagon::Database::Migration[1.0]
  def change
    create_table :organizations do |t|
      t.text :name, limit: 32, null: false
      t.text :path, limit: 32, unique: { case_insensitive: true }, null: false
      t.integer :visibility, null: false, default: 0

      t.timestamps_with_timezone
    end
  end
end
