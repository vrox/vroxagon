# frozen_string_literal: true

class CreateUserSessions < Vroxagon::Database::Migration[1.0]
  def change
    create_table :user_sessions do |t|
      t.belongs_to :user, null: false, foreign_key: { on_delete: :cascade }
      t.text :token, unique: true, null: false

      t.timestamps_with_timezone
    end
  end
end
