# frozen_string_literal: true

class CreateOrganizationMembers < Vroxagon::Database::Migration[1.0]
  def change
    create_table :organization_members do |t|
      t.belongs_to :organization, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :user, null: false, foreign_key: { on_delete: :cascade }
      t.index %i[organization_id user_id], unique: true

      t.timestamps_with_timezone
    end
  end
end
