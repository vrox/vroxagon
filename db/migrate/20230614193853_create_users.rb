# frozen_string_literal: true

class CreateUsers < Vroxagon::Database::Migration[1.0]
  def change
    create_table :users do |t|
      t.text :username, limit: 50, unique: { case_insensitive: true }, null: false
      t.text :email, limit: 255, unique: { case_insensitive: true }, null: false
      t.text :password_digest, null: false

      t.timestamps_with_timezone
    end
  end
end
