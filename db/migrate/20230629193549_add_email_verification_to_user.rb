# frozen_string_literal: true

class AddEmailVerificationToUser < Vroxagon::Database::Migration[1.0]
  def change
    change_table :users, bulk: true do |t|
      t.text :email_verification_token, unique: { allow_nil_duplicate: true }
      t.datetime_with_timezone :email_confirmed_at
    end
  end
end
