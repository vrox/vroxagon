# frozen_string_literal: true

User.seed_once do |user|
  user.id = 1
  user.username = 'root'
  user.email = 'root@vroxagon.vrox.eu'
  user.email_confirmed_at = Time.zone.now

  user.password = SecureRandom.hex(16).to_s if Rails.env.production?
  user.password = 'root' unless Rails.env.production?
end
