# frozen_string_literal: true

Rails.application.routes.draw do
  scope '-' do
    post '/graphql', to: 'graphql#execute'
    # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

    post '/users', to: 'user#new'
    get '/users/verify', to: 'user#verify_email'
    post '/user_sessions', to: 'user_session#new'
  end
end
