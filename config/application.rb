# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Vroxagon
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    Rails.application.default_url_options =
      if ENV['VROXAGON_RAILS_HOSTNAME'].nil?
        {
          host: 'localhost',
          port: 3000,
        }
      else
        {
          host: ENV['VROXAGON_RAILS_HOSTNAME'],
          port: ENV['VROXAGON_RAILS_PORT'] || 443,
          protocol: ENV['VROXAGON_RAILS_PROTOCOL'] || 'https',
        }
      end

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    config.action_mailer.preview_path = Rails.root.join('lib/mailer_previews')

    # Configure active job to use sidekiq
    config.active_job.queue_adapter = :sidekiq

    # Generated with 'bin/rails db:encryption:init'
    # Use some random generated keys, production will override this with the environment variables
    config.active_record.encryption.primary_key = ENV.fetch('VROXAGON_DATABASE_ENCRYPTION_PRIMARY_KEY',
                                                            'YzaMv4bXYK84unYIQI4Ms4sV3ucbvWs0')
    config.active_record.encryption.deterministic_key = ENV.fetch('VROXAGON_DATABASE_ENCRYPTION_DETERMINISTIC_KEY',
                                                                  'jgTaxTqzM15ved1S8HdXrqrjfCfF5R0h')
    config.active_record.encryption.key_derivation_salt = ENV.fetch('VROXAGON_DATABASE_ENCRYPTION_KEY_DERIVATION_SALT',
                                                                    'Z6zcLTgobXLYjXUslRsLMKxvXKq3j6DJ')

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    config.generators.after_generate do |files|
      parsable_files = files.filter { |file| file.end_with?('.rb') }
      system("bundle exec rubocop -A --fail-level=E #{parsable_files.shelljoin}", exception: true)
    end

    config.autoload_paths.push(Rails.root.join('lib'))
  end
end
