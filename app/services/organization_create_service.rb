# frozen_string_literal: true

class OrganizationCreateService
  attr_reader :current_user, :params

  def initialize(current_user, params)
    @current_user = current_user
    @params = params
  end

  def execute
    organization = nil
    errors = []

    ActiveRecord::Base.transaction do
      begin
        organization = Organization.create(params)
      rescue ArgumentError => e
        errors = [e.message]
        raise ActiveRecord::Rollback
      end

      unless organization.valid?
        errors = organization.errors.full_messages
        raise ActiveRecord::Rollback
      end

      organization_member = OrganizationMember.create(organization: organization, user: current_user)
      unless organization_member.valid?
        errors = organization_member.errors.full_messages
        raise ActiveRecord::Rollback
      end
    end

    unless errors.empty?
      return ServiceResponse.error(message: 'Failed to create organization',
                                   payload: errors)
    end

    ServiceResponse.success(payload: organization)
  end
end
