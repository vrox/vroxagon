# frozen_string_literal: true

class UserCreateService
  attr_reader :username, :email, :password

  def initialize(username, email, password)
    @username = username
    @email = email
    @password = password
  end

  def execute
    return conflict_response('username') if User.exists?(username: username)
    return conflict_response('email') if User.exists?(email: email)

    user = User.create(username: username, email: email, password: password)
    unless user.valid?
      return ServiceResponse.error(message: 'User is invalid', payload: user.errors,
                                   http_status: :bad_request)
    end

    UserMailer.with(user: user).registered_email.deliver_later

    ServiceResponse.success(payload: user, http_status: :created)
  end

  private

  def conflict_response(field)
    ServiceResponse.error(message: "#{field} is already taken", payload: field, http_status: :conflict)
  end
end
