# frozen_string_literal: true

class UserConfirmEmailService
  attr_reader :verification_token

  def initialize(verification_token)
    @verification_token = verification_token
  end

  def execute
    if verification_token.nil?
      return ServiceResponse.error(message: 'Missing verification token', http_status: :bad_request)
    end

    user = User.find_by(email_verification_token: verification_token)

    return ServiceResponse.error(message: 'Invalid verification token', http_status: :bad_request) if user.nil?

    user.email_verification_token = nil
    user.email_confirmed_at = Time.zone.now

    return ServiceResponse.success(message: 'User verified', payload: user) if user.save

    ServiceResponse.error(message: 'Failed to update user', payload: user, http_status: :internal_server_error)
  end
end
