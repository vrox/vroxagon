# frozen_string_literal: true

module Types
  class OrganizationMemberType < BaseObject
    description 'Represents a member of an organization'

    field :id, Types::GlobalIdType[::OrganizationMember], null: false,
                                                          description: 'GlobalID of the organization member'
    field :organization, Types::OrganizationType, description: 'Organization relating to this member'
    field :user, Types::UserType, description: 'User relating to this member'

    authorize :read_organization
  end
end
