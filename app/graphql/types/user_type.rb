# frozen_string_literal: true

module Types
  class UserType < Types::BaseObject
    description 'Represents a user of the application'

    field :id, Types::GlobalIdType[::User], null: false, description: 'GlobalID of the user'
    field :username, String, null: false, description: 'Username of the user'

    field :organizations, Types::OrganizationType.connection_type, null: false,
                                                                   description: 'Organizations where the user is member'

    authorize :read_user
  end
end
