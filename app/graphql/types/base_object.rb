# frozen_string_literal: true

module Types
  class BaseObject < GraphQL::Schema::Object
    edge_type_class Types::BaseEdge
    connection_type_class Types::CountableConnectionType
    field_class Types::BaseField

    def self.authorized?(object, context)
      subject = object.try(:declarative_policy_subject) || object

      authorize.all? do |ability|
        Ability.allowed?(context[:current_user], ability, subject)
      end
    end

    def self.authorize(*args)
      raise 'Cannot redefine authorize' if @authorize_args && args.any?

      @authorize_args = args.freeze if args.any?
      @authorize_args || (superclass.respond_to?(:authorize) ? superclass.authorize : [])
    end

    def self.id_finder(name, clazz)
      define_method(name) { |id:| VroxagonSchema.object_from_id(id, expected_type: clazz) }
    end

    def self.require_one_of(arguments, context)
      context.instance_eval do
        validates required: { one_of: arguments, message: "Only one of #{arguments.inspect} should be provided" }
      end
    end

    def id
      object.to_global_id
    end

    def current_user
      context[:current_user]
    end
  end
end
