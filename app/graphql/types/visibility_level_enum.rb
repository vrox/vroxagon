# frozen_string_literal: true

module Types
  class VisibilityLevelEnum < BaseEnum
    description 'Represents the visibility levels'

    Vroxagon::Visibility::ENUM_VALUES.each_pair do |name, value|
      value name, value: value, description: "#{name.to_s.titleize} visibility level"
    end
  end
end
