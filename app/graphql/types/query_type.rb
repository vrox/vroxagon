# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    description 'Root Query type'

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    field :echo, GraphQL::Types::String, null: false, description: 'Field available for use to test API access' do
      argument :message, GraphQL::Types::String, required: true, description: 'String to echo as response'
    end

    field :current_user, Types::UserType, null: true,
                                          description: 'Get the currently logged in user'

    field :user, Types::UserType, null: true, description: 'Find a user by id' do
      argument :id, Types::GlobalIdType[::User], required: true, description: 'GlobalID of the target user'
    end

    field :organization, Types::OrganizationType, null: true, description: 'Find an organization' do
      argument :id, Types::GlobalIdType[::Organization], required: false,
                                                         description: 'GlobalID of the target organization'
      argument :path, GraphQL::Types::String, required: false, description: 'Path of the target organization'

      BaseObject.require_one_of %i[id path], self
    end

    def echo(message:)
      message
    end

    id_finder :user, ::User

    def organization(**args)
      args[:id] = args[:id].model_id if args[:id].present?

      OrganizationsFinder.new(**args, single: true).execute
    end
  end
end
