# frozen_string_literal: true

module Types
  class OrganizationType < Types::BaseObject
    include Types::Concerns::HasVisibilityLevel

    description 'Represents a organization'

    field :id, Types::GlobalIdType[::Organization], null: false, description: 'GlobalID of the organization'
    field :name, String, null: false, description: 'Name of the organization'
    field :path, String, null: false, description: 'Path of the organization'

    field :members, [Types::OrganizationMemberType], description: 'Members of the organization',
                                                     method: :organization_members
    field :users, [Types::UserType], description: 'Users that are members of the organization'

    authorize :read_organization
  end
end
