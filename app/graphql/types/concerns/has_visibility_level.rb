# frozen_string_literal: true

module Types
  module Concerns
    module HasVisibilityLevel
      extend ActiveSupport::Concern

      included do
        field :visibility, Types::VisibilityLevelEnum, null: false, description: 'The configured visibility level'
      end

      def visibility
        Vroxagon::Visibility::ENUM_VALUES[object.visibility.to_sym]
      end
    end
  end
end
