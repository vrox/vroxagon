# frozen_string_literal: true

module Mutations
  class CreateOrganization < BaseMutation
    description 'Create a new organization'

    field :organization, Types::OrganizationType, null: true, description: 'The created organization'

    argument :name, String, required: true, description: 'The name of the organization'
    argument :path, String, required: true, description: 'The path of the organization'
    argument :visibility, Types::VisibilityLevelEnum, required: true, description: 'The visiblity of the organization'

    def resolve(args)
      OrganizationCreateService.new(current_user, args).execute.to_mutation_response(success_key: :organization)
    end
  end
end
