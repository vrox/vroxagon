# frozen_string_literal: true

class Organization < ApplicationRecord
  enum :visibility, Vroxagon::Visibility::ENUM_VALUES, prefix: :visibility

  has_many :organization_members, class_name: 'OrganizationMember', inverse_of: :organization
  has_many :users, class_name: 'User', through: :organization_members

  validates :name, presence: true, allow_blank: false
  validates :path, presence: true,
                   allow_blank: false,
                   uniqueness: { case_sensitive: false }
  validates :visibility, presence: true,
                         inclusion: {
                           in: Vroxagon::Visibility::ENUM_VALUES.keys.map(&:to_s),
                         }
end
