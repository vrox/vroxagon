# frozen_string_literal: true

class OrganizationMemberRoleAssignment < ApplicationRecord
  enum :role, Vroxagon::Role::ENUM_VALUES

  belongs_to :organization_member, inverse_of: :roles
  belongs_to :created_by, class_name: 'User', inverse_of: :created_organization_member_role_assignments

  validates :role, presence: true,
                   inclusion: {
                     in: Vroxagon::Role::ENUM_VALUES.keys.map(&:to_s),
                   }
end
