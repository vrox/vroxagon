# frozen_string_literal: true

module Ability
  module_function

  def allowed?(user, ability, subject = :global)
    policy = policy_for(user, subject)

    policy.allowed?(ability)
  ensure
    forget_runner_result(policy.runner(ability)) if policy
  end

  def policy_for(user, subject = :global)
    Cache.policies ||= {}

    DeclarativePolicy.policy_for(user, subject, cache: Cache.policies)
  end

  def forget_runner_result(runner)
    runner.instance_variable_set(:@state, nil)
  end

  class Cache < ActiveSupport::CurrentAttributes
    attribute :policies
  end
end
