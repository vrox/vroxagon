# frozen_string_literal: true

class OrganizationMember < ApplicationRecord
  belongs_to :organization, inverse_of: :organization_members
  belongs_to :user, inverse_of: :organization_memberships

  has_many :roles, class_name: 'OrganizationMemberRoleAssignment', inverse_of: :organization_member

  Vroxagon::Role::ENUM_VALUES.entries.each_entry do |role, value|
    define_method("#{role}?") do
      roles.exists?(role: value)
    end
  end
end
