# frozen_string_literal: true

class User < ApplicationRecord
  include TokenAttr

  has_secure_password

  token_attr :email_verification_token, prefix: 'v_evt_', allow_nil: true

  validates :username, length: { maximum: 50 },
                       presence: true,
                       allow_blank: false,
                       uniqueness: { case_sensitive: false }
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP },
                    presence: true,
                    allow_blank: false,
                    uniqueness: { case_sensitive: false }

  has_many :user_sessions, inverse_of: :user
  has_many :organization_memberships, class_name: 'OrganizationMember', inverse_of: :user
  has_many :organizations, class_name: 'Organization', through: :organization_memberships
  has_many :created_organization_member_role_assignments, class_name: 'OrganizationMemberRoleAssignment',
                                                          inverse_of: :created_by

  # rubocop:disable Metrics/CyclomaticComplexity
  # rubocop:disable Metrics/PerceivedComplexity
  def self.authenticate_by(attributes)
    passwords, identifiers = attributes.to_h.partition do |name, _value|
      !has_attribute?(name) && has_attribute?("#{name}_digest")
    end.map(&:to_h)

    raise ArgumentError, 'One or more password arguments are required' if passwords.empty?
    raise ArgumentError, 'One or more finder arguments are required' if identifiers.empty?

    if (record = find_by(identifiers))
      record if passwords.count { |name, value| record.public_send(:"authenticate_#{name}", value) } == passwords.size
    else
      new(passwords)
      nil
    end
  end
  # rubocop:enable Metrics/PerceivedComplexity
  # rubocop:enable Metrics/CyclomaticComplexity

  def email_confirmed?
    email_confirmed_at.present? && email_verification_token.nil?
  end
end
