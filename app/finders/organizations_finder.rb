# frozen_string_literal: true

class OrganizationsFinder < ApplicationFinder
  attr_reader :params

  def initialize(params)
    super()
    @params = params
  end

  def execute
    organizations = base_scope
    organizations = by_id(organizations)
    organizations = by_path(organizations)

    super(organizations)
  end

  private

  def base_scope
    Organization.all
  end

  def by_id(organizations)
    return organizations unless params[:id]

    organizations.where(id: params[:id])
  end

  def by_path(organizations)
    return organizations unless params[:path]

    organizations.where(path: params[:path])
  end
end
