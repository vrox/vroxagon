# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: email_address_with_name('no-reply@vrox.eu', 'Vroxagon')
  layout 'mailer'
end
