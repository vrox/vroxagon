# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def registered_email
    @user = params[:user]
    mail to: @user.email, subject: 'Verify email'
  end
end
