# frozen_string_literal: true

class UserController < ApplicationController
  def new
    username = allowed_register_params[:username]
    email = allowed_register_params[:email]
    password = allowed_register_params[:password]

    response = ::UserCreateService.new(username, email, password).execute

    return render json: response.payload, status: response.http_status if response.success?

    render json: { reason: response.message, errors: response.payload }, status: response.http_status
  end

  def verify_email
    token = allowed_verify_params[:token]

    response = UserConfirmEmailService.new(token).execute

    return redirect_to '/' if response.success?

    render json: { reason: response.message }, status: response.http_status
  end

  private

  def allowed_register_params
    params.require(:user).permit(:username, :email, :password)
  end

  def allowed_verify_params
    params.require(:user).permit(:token)
  end
end
