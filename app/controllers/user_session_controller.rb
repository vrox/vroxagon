# frozen_string_literal: true

class UserSessionController < ApplicationController
  def new
    username = allowed_params[:username]
    password = allowed_params[:password]
    user = User.authenticate_by(username: username, password: password)
    return head :unauthorized if user.nil?
    return render json: { reason: :unverified_email }, status: :forbidden unless user.email_confirmed?
    return render json: { reason: :policy_denied }, status: :forbidden unless Ability.allowed?(user, :log_in)

    render json: UserSession.create(user: user)
  end

  private

  def allowed_params
    params.require(:user).permit(:username, :password)
  end
end
