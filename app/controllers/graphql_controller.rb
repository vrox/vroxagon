# frozen_string_literal: true

class GraphqlController < ApplicationController
  # If accessing from outside this domain, nullify the session
  # This allows for outside API access while preventing CSRF attacks,
  # but you'll have to authenticate your user separately
  # protect_from_forgery with: :null_session

  # rubocop:disable Metrics/CyclomaticComplexity
  # rubocop:disable Metrics/PerceivedComplexity
  def execute
    authorization_token = request.headers['Authorization']

    current_authorization = find_authorization(authorization_token)

    return head :unauthorized if authorization_token.present? == current_authorization.none?
    return head :unauthorized if current_authorization.invalid?
    return head :forbidden if !current_authorization.mutations_allowed? && mutation?

    current_user = current_authorization.authorization&.user
    return head :forbidden unless Ability.allowed?(current_user, :access_api)

    variables = prepare_variables(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    context = {
      current_user: current_user,
    }
    result = VroxagonSchema.execute(query, variables: variables, context: context, operation_name: operation_name)
    render json: result
  rescue StandardError => e
    raise e unless Rails.env.development?

    handle_error_in_development(e)
  end
  # rubocop:enable Metrics/PerceivedComplexity
  # rubocop:enable Metrics/CyclomaticComplexity

  private

  # Handle variables in form data, JSON body, or a blank value
  def prepare_variables(variables_param)
    case variables_param
    when String
      if variables_param.present?
        JSON.parse(variables_param) || {}
      else
        {}
      end
    when Hash
      variables_param
    when ActionController::Parameters
      variables_param.to_unsafe_hash # GraphQL-Ruby will validate name and type of incoming variables.
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{variables_param}"
    end
  end

  def find_authorization(authorization)
    return Authorization.new(:none, nil) if authorization.blank?

    token_type, token = authorization.split(' ', 2)

    case token_type
    when 'Session'
      Authorization.new(:session, UserSession.find_by(token: token))
    else
      Authorization.new(:invalid, nil)
    end
  end

  def mutation?(query_string = params[:query], operation_name = params[:operationName])
    ::GraphQL::Query.new(VroxagonSchema, query_string, operation_name: operation_name).mutation?
  end

  def handle_error_in_development(e)
    logger.error e.message
    logger.error e.backtrace.join("\n")

    render json: { errors: [{ message: e.message, backtrace: e.backtrace }], data: {} }, status: :internal_server_error
  end

  Authorization = Struct.new(:type, :authorization) do
    def mutations_allowed?
      return true if session?

      false
    end

    def none?
      type == :none
    end

    def invalid?
      type == :invalid
    end

    def session?
      type == :session
    end
  end
end
