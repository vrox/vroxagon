# frozen_string_literal: true

class OrganizationPolicy < BasePolicy
  condition(:public_visibility, scope: :subject) { @subject.visibility_public? }
  condition(:private_visibility, scope: :subject) { @subject.visibility_private? }

  condition(:user_is_member) { OrganizationMember.exists?(organization: @subject, user: @user) }

  rule { public_visibility }.policy do
    enable :read_organization
  end

  rule { user_is_member }.policy do
    enable :read_organization
  end

  rule { private_visibility & ~user_is_member }.policy do
    prevent :read_organization
  end
end
