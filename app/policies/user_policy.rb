# frozen_string_literal: true

class UserPolicy < BasePolicy
  rule { default }.enable :read_user
end
