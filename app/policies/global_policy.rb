# frozen_string_literal: true

class GlobalPolicy < BasePolicy
  condition(:email_confirmed, scope: :user) { @user.nil? || @user.email_confirmed? }

  rule { default }.policy do
    enable :log_in
    enable :access_api
  end

  rule { ~email_confirmed }.policy do
    prevent :log_in
    prevent :access_api
  end

  rule { anonymous }.prevent :log_in
end
