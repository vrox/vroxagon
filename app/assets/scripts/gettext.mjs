import fs from "fs";
import path from "path";

const __dirname = path.resolve("src");

function getFiles(targetDirectory) {
    const directoryPath = path.join(__dirname, targetDirectory);
    return readDirectoryRecursive(directoryPath, `${__dirname}/${targetDirectory}`);
}

function readDirectoryRecursive(directory, currentPath) {
    const dirents = fs.readdirSync(directory, { withFileTypes: true });

    const files = dirents.filter(dirent => dirent.isFile()).map(dirent => `${dirent.name}`);
    const directories = dirents.filter(dirent => dirent.isDirectory());
    directories.forEach(dir => {
        const recursedFiles = readDirectoryRecursive(path.join(currentPath, dir.name), `${currentPath}/${dir.name}`).map(filename => `${dir.name}/${filename}`);
        files.push(...recursedFiles);
    });
    return files;
}

function readFile(file) {
    return fs.readFileSync(path.resolve(__dirname, file), "utf-8");
}

const files = getFiles(".");
const mergedContent = files.map(readFile).join("\n");

const translateCallMatches = [...mergedContent.matchAll(/\btc?\(["'](.*?)["'](?:,.+)?\)/g)]
    .concat([...mergedContent.matchAll(/i18n: *["'](.*?)["']/g)])
    .concat([...mergedContent.matchAll(/\/\* *i18n *\*\/ *["'](.*?)["']/g)]);

const translationKeys = [...new Set(translateCallMatches.map(t => t[1]).sort())];

const localeFiles = getFiles("locales");
const localeData = localeFiles.map(file => {
    let json;
    try {
        json = JSON.parse(readFile(`locales/${file}`));
    } catch (e) {
        json = {};
    }
    return {file: `locales/${file}`, data: json};
});

let addedKeys = 0;
let reusedKeys = 0;

localeData.forEach(locale => {
    const translations = generateLocaleData(translationKeys, locale);
    fs.writeFileSync(path.resolve(__dirname, locale.file), `${JSON.stringify(translations, null, 2)}\n`, "utf-8");
});

function removeContext(translation) {
    return translation.replace(/\S+\|(.*)/, "$1")
}

function generateLocaleData(translationKeys, locale) {
    return translationKeys.reduce((acc, key) => {
        if (locale.data[key]) {
            acc[key] = locale.data[key];
            reusedKeys++;
        } else {
            acc[key] = removeContext(key);
            addedKeys++;
        }
        return acc;
    }, {});
}

const totalTranslations = localeData.map(l => Object.keys(l.data).length).reduce((acc, cur) => acc + cur, 0);

console.info(`Added ${addedKeys} translations`);
console.info(`Re-used ${reusedKeys} translations`);
console.info(`Removed ${totalTranslations - reusedKeys} translations`);
