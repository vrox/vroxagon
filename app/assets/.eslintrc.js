module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
    ],
    rules: {
        "no-restricted-imports": ["error", {
            paths: [{
                name: "vue-i18n",
                message: "Import from @/plugins/i18n instead"
            }],
        }]
    }
}
