import App from './App.vue'

import { createApp } from 'vue'

import { registerPlugins } from '@/plugins'
import {loadMessages} from "@/plugins/i18n";

const app = createApp(App)

// noinspection JSIgnoredPromiseFromCall
loadMessages('en') // only en supported for now

registerPlugins(app)

app.mount('#app')
