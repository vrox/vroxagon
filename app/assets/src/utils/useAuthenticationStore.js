import {defineStore} from "pinia";
import {computed} from "vue";
import {useLocalStorage} from "@vueuse/core";

export const useAuthenticationStore = defineStore('authentication', () => {
    const session = useLocalStorage('vroxagon:authentication', {}, {
        serializer: {
            read: v => v ? JSON.parse(v) : null,
            write: v => JSON.stringify(v)
        }
    })

    const token = computed(() => session.value?.token);
    const isLoggedIn = computed(() => !!token.value);

    return { session, token, isLoggedIn };
})
