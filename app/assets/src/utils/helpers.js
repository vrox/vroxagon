import {identity} from "ramda";

const titleSeparator = ' · ';

const applicationName = 'Vroxagon';

export const toTitle = (parts) => ([...parts, applicationName].filter(identity).join(titleSeparator))
