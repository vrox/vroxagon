import {nextTick} from "vue";
// eslint-disable-next-line no-restricted-imports
import {createI18n} from 'vue-i18n'

const i18n = createI18n({
    legacy: false,
    locale: 'en',
    fallbackLocale: 'en',
})
export default i18n;

export async function loadMessages(locale) {
    const messages = await import(`../locales/${locale}.json`);

    i18n.global.setLocaleMessage(locale, messages.default)

    return nextTick();
}

const removeContext = (translation) => translation.replace(/\S+\|(.*)/, "$1")

export const useI18n = () => {
    const t = (...args) => removeContext(i18n.global.t(...args));
    const tc = (...args) => removeContext(i18n.global.tc(...args));

    return { t, tc };
}
