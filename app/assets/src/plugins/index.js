import {loadFonts} from './webfontloader'
import vuetify from './vuetify'
import pinia from './pinia'
import router from './routes'
import i18n from './i18n';
import {createApollo} from "@/plugins/apollo";
import {provideApolloClient} from "@vue/apollo-composable";

export function registerPlugins(app) {
    loadFonts()
    app
        .use(vuetify)
        .use(pinia)
        .use(router)
        .use(i18n)

    provideApolloClient(createApollo());
}
