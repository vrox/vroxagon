import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'index',
        component: () => import('@/views/HomeView.vue'),
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/LoginView.vue'),
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/views/RegisterView.vue'),
    },
    {
        path: '/organizations/:path',
        name: 'organization',
        component: () => import('@/views/OrganizationView.vue'),
        children: [
            {
                path: '',
                name: 'organization-home',
                component: () => import('@/views/organization/OrganizationHomeView.vue')
            }
        ]
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

export default router
