import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client/core'
import {ApolloLink, concat, from} from "@apollo/client/link/core";
import {onError} from "@apollo/client/link/error";
import {logErrorMessages} from '@vue/apollo-util';
import {storeToRefs} from "pinia";
import {useAuthenticationStore} from "@/utils/useAuthenticationStore";
import {watch} from "vue";

export function createApollo() {
    const { token } = storeToRefs(useAuthenticationStore());

    const errorLink = onError((error) => {
        if(import.meta.env.NODE_ENV !== 'production') {
            logErrorMessages(error)
        }
    });

    const authMiddleware = new ApolloLink((operation, forward) => {
        if (token.value) {
            operation.setContext({
                headers: {
                    authorization: `Session ${token.value}`
                }
            })
        }
        return forward(operation);
    });

    const httpLink = createHttpLink({uri: '/-/graphql'});

    const cache = new InMemoryCache();

    const client = new ApolloClient({
        link: from([errorLink, concat(authMiddleware, httpLink)]),
        defaultOptions: {
            query: {
                errorPolicy: 'all'
            },
            mutate: {
                errorPolicy: 'all'
            },
            watchQuery: {
                errorPolicy: 'all'
            },
        },
        cache
    });

    watch(token, () => client.refetchQueries({ include: 'all' }));

    return client;
}
