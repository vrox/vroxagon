# frozen_string_literal: true

class UserMailerPreview < ActionMailer::Preview
  def registered_email
    UserMailer.with(user: User.first).registered_email
  end
end
