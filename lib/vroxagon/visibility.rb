# frozen_string_literal: true

module Vroxagon
  class Visibility
    PUBLIC = 0
    PRIVATE = 10

    ENUM_VALUES = {
      public: PUBLIC,
      private: PRIVATE,
    }.freeze
  end
end
