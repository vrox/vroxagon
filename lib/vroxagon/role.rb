# frozen_string_literal: true

module Vroxagon
  class Role
    MINIMAL_ACCESS = 0
    ORGANIZATION_ADMIN = 1000

    ENUM_VALUES = {
      minimal_access: MINIMAL_ACCESS,
      organization_admin: ORGANIZATION_ADMIN,
    }.freeze
  end
end
